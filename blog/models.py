from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.utils.text import slugify
from ckeditor_uploader.fields import RichTextUploadingField
from PIL import Image
from taggit.managers import TaggableManager


class Article(models.Model):
    title = models.CharField(max_length=100)
    images = models.ImageField(upload_to='imgs/')
    slug = models.SlugField(default='', editable=False, max_length=500)
    content = RichTextUploadingField()
    creation_date = models.DateTimeField(default=timezone.now)
    taggit = TaggableManager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
    	return reverse('post-detail', kwargs={'slug': self.slug,})

    def save(self, *args, **kwargs):
    	value = self.title
    	self.slug = slugify(value, allow_unicode=True)
    	super().save(*args, **kwargs)

    class Meta:
        ordering = ['-creation_date']
