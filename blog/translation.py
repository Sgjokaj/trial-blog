from modeltranslation.translator import (
    translator,
    register,
    TranslationOptions,
)
from .models import Article

class ArticleTranslationsOptions(TranslationOptions):
    fields = ('title', 'content')


translator.register(Article, ArticleTranslationsOptions)